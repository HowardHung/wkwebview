//
//  HWBaseWebViewController.m
//  WebView封装
//
//  Created by Hanwen on 2017/12/28.
//  Copyright © 2017年 SK丿希望. All rights reserved.
//

#import "HWBaseWebViewController.h"
#import "QRcodeViewController.h"

@interface HWBaseWebViewController ()

/** <#注释#> */
@property(nonatomic, strong) UIView *mainView;
/** <#注释#> */
@property(nonatomic, strong) UIProgressView *progressView;
@end

@implementation HWBaseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addMainView];
    [self creatWebView];
    self.view.backgroundColor = [UIColor whiteColor];
    NSLog(@"token:%@",self.token);
}
#pragma mark - 添加进度条
- (void)addMainView {
    HWWeakSelf(weakSelf)
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HWScreenW, HWScreenH )];
    mainView.backgroundColor = [UIColor clearColor];
    _mainView = mainView;
    [weakSelf.view addSubview:mainView];
    UIProgressView *progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, HWScreenW, 1)];
    progressView.progress = 0;
    _progressView = progressView;
    [weakSelf.view addSubview:progressView];
}
- (void)creatWebView {
    
    HWWeakSelf(weakSelf)
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.userContentController = [[WKUserContentController alloc] init];
    // 注入JS对象Native，
    // 声明WKScriptMessageHandler 协议
    [config.userContentController addScriptMessageHandler:weakSelf name:@"Native"];
    //本人喜欢只定义一个MessageHandler协议 当然可以定义其他MessageHandler协议
    [config.userContentController addScriptMessageHandler:weakSelf name:@"Pay"];
    
    _webView =  [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, HWScreenW, HWScreenH ) configuration:config];
   
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.navigationDelegate = weakSelf;
    _webView.UIDelegate = weakSelf ;
    _webView.scrollView.bounces = NO;
    [weakSelf.mainView addSubview:_webView];
    // 添加观察者
    [_webView addObserver:weakSelf forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL]; // 进度
    [_webView addObserver:weakSelf forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL]; // 标题
}
#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    NSDictionary *bodyParam = (NSDictionary*)message.body;
    NSString *func = [bodyParam objectForKey:@"function"];
    
    NSLog(@"MessageHandler Name:%@", message.name);
    NSLog(@"MessageHandler Body:%@", message.body);
    NSLog(@"MessageHandler Function:%@",func);
    
    //本人喜欢只定义一个MessageHandler协议 当然可以定义其他MessageHandler协议
    
    if ([message.name isEqualToString:@"Native"])
    {
        NSDictionary *parameters = [bodyParam objectForKey:@"parameters"];
        //调用本地函数1
        if([func isEqualToString:@"addSubView"])
        {
            Class tempClass =  NSClassFromString([parameters objectForKey:@"view"]);
            CGRect frame = CGRectFromString([parameters objectForKey:@"frame"]);
            
            if(tempClass && [tempClass isSubclassOfClass:[UIWebView class]])
            {
                UIWebView *tempObj = [[tempClass alloc] initWithFrame:frame];
                tempObj.tag = [[parameters objectForKey:@"tag"] integerValue];
                
                NSURL *url = [NSURL URLWithString:[parameters objectForKey:@"urlstring"]];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [tempObj loadRequest:request];
                [self.webView addSubview:tempObj];
                
            }
        }
        //调用本地函数2
        else if([func isEqualToString:@"alert"])
        {
            [self showMessage:@"來自網頁的提示" message:[parameters valueForKey:@"message"] ];
        }
        //调用本地函数3
        else if([func isEqualToString:@"iOSScan"])
        {
             UIStoryboard *storyboard ;
               
               if (storyboard == nil) {
                   
                   storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                   QRcodeViewController *iOSScan = [storyboard instantiateViewControllerWithIdentifier:@"iOSScan"];
                   [self.navigationController pushViewController:iOSScan animated:YES];
               }
        }
        //调用本地函数4
        else if([func isEqualToString:@"finishWebLoad"])
        {
            if (self.token.length > 0) {
            
//                NSLog(@"1234567");
                [self sendTokenFromApp];
                
            }
        }
        else if([func isEqualToString:@"backToApp"])
        {
            [self backToApp];
        }
    }else if ([message.name isEqualToString:@"Pay"])
    {
        //如果是自己定义的协议, 再截取协议中的方法和参数, 判断无误后在这里进行逻辑处理
        
    } else if ([message.name isEqualToString:@"dosomething"])
    {
        //........
    }
}
-(void)iOSRequest:(NSString *)function {
    
    NSString *parm = [NSString stringWithFormat:@"iOSRequest('%@')",[[function stringByReplacingOccurrencesOfString:@"\r"
    withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""] ];
    
    [self.webView evaluateJavaScript:parm  completionHandler:^(id item, NSError * _Nullable error) {
        
        NSLog(@"iOSRequest:%@",parm);
    }];
}
-(void)sendTokenFromApp {
    
    NSString *parm = [NSString stringWithFormat:@"sendTokenFromApp('%@')",[[[self.token stringByReplacingOccurrencesOfString:@"\r"
    withString:@""] stringByReplacingOccurrencesOfString:@"\n"
    withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    [self.webView evaluateJavaScript:parm  completionHandler:^(id item, NSError * _Nullable error) {
        
        NSLog(@"sendTokenFromApp:%@",parm);
    }];
}
-(void)backToApp {
    
    NSLog(@"back");
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showMessage:(NSString *)title message:(NSString *)message;
{
    if (message == nil)
    {
        return;
    }
    [self alertControl:title :message];
}
- (void)setUrl:(NSURL *)url {
    _url = url;
    HWWeakSelf(weakSelf)
    NSURLRequest *request = [NSURLRequest requestWithURL:weakSelf.url];
    [_webView loadRequest:request];
}
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
    
    NSLog(@"當網絡視圖開始接收網絡內容");
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    
    NSLog(@"當Web內容開始加載到Web視圖中");
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
    [self alertControl:@"頁面加載失败" :nil];
    
    NSLog(@"頁面加載失败");
}
// 页面加载完毕时调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    
    NSLog(@"頁面加載完畢");

    
}
#pragma mark - 监听加载进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    HWWeakSelf(weakSelf)
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object == _webView) {
            [weakSelf.progressView setAlpha:1.0f];
            [weakSelf.progressView setProgress:weakSelf.webView.estimatedProgress animated:YES];
            if(weakSelf.webView.estimatedProgress >= 1.0f) {
                [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [weakSelf.progressView setAlpha:0.0f];
                } completion:^(BOOL finished) {
                    [weakSelf.progressView setProgress:0.0f animated:NO];
                }];
            }
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }  else if ([keyPath isEqualToString:@"title"]) {
        HWLog(@"%@",weakSelf.webView.title);
        if (object == weakSelf.webView) {
            weakSelf.title = weakSelf.webView.title;
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
#pragma mark - WKUIDelegate
- (void)webViewDidClose:(WKWebView *)webView {
    NSLog(@"webViewDidClose:%s", __FUNCTION__);
}
//通過category可以攔截alert
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    
    NSLog(@"攔截網頁Alert:%s", __FUNCTION__);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:NULL];
}
//通過category可以攔截網頁Confirm
-(void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    
//    NSLog(@"攔截網頁Confirm:%s", __FUNCTION__);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"登出" message:@"是否登出返回主頁" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        completionHandler(NO);
    }];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
//通過category可以攔截網頁輸入框
-(void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    
    NSLog(@"攔截網頁輸入框:%s", __FUNCTION__);
}
// Webview銷毀的时候调用
- (void)dealloc {
    NSLog(@"webView释放");
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [_webView removeObserver:self forKeyPath:@"title"];
    _webView.navigationDelegate = nil;
    _webView.UIDelegate = nil ;
}

//警示視窗都用這
-(void)alertControl :(NSString *)title :(NSString *)message {
    
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:title
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    // Close
    UIAlertAction *alertAction =
    [UIAlertAction actionWithTitle:@"確定"
                             style:UIAlertActionStyleCancel
                           handler:nil];
    [alertController addAction:alertAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
#pragma mark - WKNavigationDelegate
#pragma mark - 截取当前加载的URL
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
//    HWWeakSelf(weakSelf)
//    NSURL *URL = navigationAction.request.URL;
//    HWLog(@"%@", URL);
//    if (![[NSString stringWithFormat:@"%@", weakSelf.url] isEqualToString:[NSString stringWithFormat:@"%@", URL]]) { // 不相等
//        //        weakSelf.navigationView.titleLabel.text = @"攻略详情";
//        HWStrategyDetailsViewController *vc = [[HWStrategyDetailsViewController alloc] init];
//        vc.url = URL;
//        [weakSelf.navigationController pushViewController:vc animated:YES];
//        //        self.button.hidden = NO;
//        //        _webView.height = HWScreenH-64-50;
//        //        self.collectionButton.hidden = NO;
//        //        self.forwardingButton.hidden = NO;
//        decisionHandler(WKNavigationActionPolicyCancel);
//    }else {
//        weakSelf.navigationView.titleLabel.text = weakSelf.title;
//        decisionHandler(WKNavigationActionPolicyAllow); // 必须实现 不然会崩溃
//    }
//}
@end
