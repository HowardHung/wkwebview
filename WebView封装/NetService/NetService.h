//
//  NetService.h
//  DigitalHR
//
//  Created by tkbg on 2018/9/4.
//  Copyright © 2018年 wistron.HRMS.DigitalHR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

////目標API


//#define Annual_Accumulate_API @"Manager_Subordinate_Annual_Accumulates"

//req Success block
typedef void (^requestSuccessBlock)(NSDictionary *dic);

//req fail block
typedef void (^requestFailureBlock)(NSError *error);

//req define
typedef enum {
    GET,
    POST,
    PUT,
    DELETE,
    HEAD
} HTTPMethod;

@interface NetService : AFHTTPSessionManager <NSURLSessionDelegate>

+(instancetype)sharedManager;
+(NSString*)dictionaryToJson:(NSDictionary *)dic;
+(NSString*)dictionaryToString:(NSDictionary *)dic;
+(NSDictionary*)dictionaryWithJsonString:(NSString *)jsonString;

- (void)requestWithMethod:(HTTPMethod)method
                 WithPath:(NSString *)path
               WithParams:(NSString*)params
         WithSuccessBlock:(requestSuccessBlock)success
          WithFailurBlock:(requestFailureBlock)failure;
//改過的GET
-(void)requestAPIWithGET :(NSString *)whichAPI
            Json:(NSString *)json
WithSuccessBlock:(requestSuccessBlock)success
 WithFailurBlock:(requestFailureBlock)failure;
//改過的POST
-(void)requestAPIWithPOST:(NSString *)whichAPI
               WithParams:(NSDictionary*)params
         WithSuccessBlock:(requestSuccessBlock)success
          WithFailurBlock:(requestFailureBlock)failure;

-(void)showWaitting ;
- (void)showSuccess ;

- (void)showError ;
- (void)hideHUD ;
- (void)showMessage :(NSString *)message ;
//- (void)nSURLSessionRequest :(NSString *)url ;
@end
