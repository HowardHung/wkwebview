//
//  NetService.m
//  DigitalHR
//
//  Created by tkbg on 2018/9/4.
//  Copyright © 2018年 wistron.HRMS.DigitalHR. All rights reserved.
//
#import "NetService.h"
#import "LCProgressHUD.h"
#define TIME 15.0f

////測試環境網址
NSString *const DefaultHost = @"http://bpmap01-qas.whq.wistron:3000";
////目錄
NSString *const FOLDER  = @"/api/" ;
////參數
NSString *const Filter = @"filter=" ;
@implementation NetService
{
    NSMutableArray *requestArray ;
}

+ (instancetype)sharedManager {
    static NetService *manager = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        manager = [[self alloc] init];
    });
    return manager;
}
- (instancetype)init;
{
    self = [super init];
    if (self) {
        self.requestSerializer.timeoutInterval = 15;
        self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [self.securityPolicy setAllowInvalidCertificates:NO];
        [self.responseSerializer setAcceptableContentTypes:[NSSet setWithObject:@"application/json"]];
    }
    return self;
}
//原始的request
- (void)requestWithMethod:(HTTPMethod)method WithPath:(NSString *)path
               WithParams:(NSString*)params
         WithSuccessBlock:(requestSuccessBlock)success
          WithFailurBlock:(requestFailureBlock)failure
{
    switch (method) {
            
        case GET:{
            
            [self showWaitting];

            NSLog(@"path:%@",path);
            NSLog(@"accesstokenDic:%@",params);
            [self GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                success(responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                [self showError];
                failure(error);
            }];
            
            break;
                 }
        case POST:{
            
            [self showWaitting];

            [self POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                success(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                [self showError];
                failure(error);
            }];
            
            break;
        }
        default:
            
            break;
    }
}
//加工過的GET
-(void)requestAPIWithGET :(NSString *)whichAPI
                     Json:(NSString *)json
         WithSuccessBlock:(requestSuccessBlock)success
          WithFailurBlock:(requestFailureBlock)failure
{
    [self showWaitting];

    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"accessToken"] != nil) {
        
        NSDictionary *accesstokenDic = [NSDictionary dictionaryWithObject:
                                        [[NSUserDefaults standardUserDefaults]valueForKey:@"accessToken"] forKey:@"access_token"];
        
        NSString *url ;
        if (json == nil) {
            
            url = [NSString stringWithFormat:
                   @"%@%@%@",DefaultHost,FOLDER,whichAPI];
        }else{
            url = [NSString stringWithFormat:
                   @"%@%@%@?%@%@",DefaultHost,FOLDER,whichAPI,Filter,[json stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        }
        NSLog(@"url:%@",url);
        NSLog(@"accesstokenDic:%@",accesstokenDic);
        [self GET:url parameters:accesstokenDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
//                    NSLog(@"responseObject:%@",responseObject);
                    success(responseObject);
                    
                } failure:^(NSURLSessionTask *operation, NSError *error) {
                    
                    [self showError];
                    NSLog(@"error.description:%@",error.description);
                    failure(error);
                }];
    }
}
//加工過的POST
-(void)requestAPIWithPOST:(NSString *)whichAPI
                WithParams:(NSDictionary*)params
         WithSuccessBlock:(requestSuccessBlock)success
          WithFailurBlock:(requestFailureBlock)failure
{
    [self showWaitting];

    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"accessToken"] != nil) {
        
        NSString *access_token = [NSString stringWithFormat:@"?access_token=%@",
                                  [[NSUserDefaults standardUserDefaults]valueForKey:@"accessToken"]];
        
        NSString *url = [NSString stringWithFormat:@"%@%@%@%@",DefaultHost,FOLDER,whichAPI,access_token];;
        
        [self POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

            success(responseObject);

        } failure:^(NSURLSessionTask *operation, NSError *error) {

            [self showError];
            failure(error);
        }];
    }
}
#pragma LCProgressHUD
-(void)showWaitting {
    
    [LCProgressHUD showLoading:@"Loading..."];
    [NSTimer scheduledTimerWithTimeInterval:TIME
                                     target:self
                                   selector:@selector(hideHUD)
                                   userInfo:nil
                                    repeats:NO];
    
}
- (void)showSuccess {
    
    [LCProgressHUD showSuccess:@"Loading Success"];
}

- (void)showError {
    
    [LCProgressHUD showFailure:@"Loading Error"];
}
- (void)hideHUD {
    
    [LCProgressHUD hide];
}
- (void)showMessage :(NSString *)message{
    
    [LCProgressHUD showInfoMsg:message];
}
//dictionary轉Json
+(NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
//dictionary轉String
+(NSString*)dictionaryToString:(NSDictionary *)dic
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dic options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    return myString;
}
//JSON转NSDictionary
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

//- (void)nSURLSessionRequest :(NSString *)url{
//
//        //創建請求
//        NSURL *requestUrl = [NSURL URLWithString:url];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl];
//        //設置request的緩存策略（決定該request是否要從緩存中獲取）
//        request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
//        //創建配置（決定要不要將數據和響應緩存在磁盤）
//        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//        //configuration.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
//        //創建會話
//        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//        //生成任務
//        NSURLSessionDataTask *task = [session dataTaskWithRequest:request];
//        //創建的task是停止狀態，需要我們去啟動
//        [task resume];
//}
//1.接收到服務器響應的時候調用
//- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
//didReceiveResponse:(NSURLResponse *)response
// completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler{
//    NSLog(@"接收響應");
//    //必須告訴系統是否接收服務器返回的數據
//    //默認是completionHandler(NSURLSessionResponseAllow)
//    //可以再這邊通過響應的statusCode來判斷否接收服務器返回的數據
//    completionHandler(NSURLSessionResponseAllow);
//}
//2.接受到服務器返回數據的時候調用,可能被調用多次
//- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
//    //一般在這邊進行數據的拼接，在方法3才將完整數據回調
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    NSLog(@"接收到數據:%@",dic);
//}
//3.請求完成或者是失敗的時候調用
//- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)taskdidCompleteWithError:(nullable NSError *)error{
//    NSLog(@"請求完成或者是失敗");
//    //在這邊進行完整數據的解析，回調
//}
//4.將要緩存響應的時候調用（必須是默認會話模式，GET請求才可以）
//- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
// willCacheResponse:(NSCachedURLResponse *)proposedResponse
// completionHandler:(void (^)(NSCachedURLResponse * _Nullable cachedResponse))completionHandler{
//    //可以在這邊更改是否緩存，默認的話是completionHandler(proposedResponse)
//    //不想緩存的話可以設置completionHandler(nil)
//    completionHandler(proposedResponse);
//}
@end
