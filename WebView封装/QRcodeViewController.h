//
//  QRcodeViewController.h
//  WebView封装
//
//  Created by HowardHung on 2019/10/21.
//  Copyright © 2019 SK丿希望. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface QRcodeViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>
{
 IBOutlet UILabel *statementLabel;
 IBOutlet UIView *scanView;
}
 
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;
 
-(BOOL)startReading;
-(void)stopReading;
 
@end

