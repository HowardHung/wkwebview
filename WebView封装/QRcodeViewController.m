/**
Theme: Scan QR Code & Bar Code
IDE: Xcode 7
Language: Objective C
Date: 104/11/19
Author: HappyMan
Blog: https://cg2010studio.wordpress.com/
*/
#import "QRcodeViewController.h"
#import "oneViewController.h"
@interface QRcodeViewController ()

@end

@implementation QRcodeViewController

#pragma mark - IBAction method implementation
- (void)viewDidLoad {
    [super viewDidLoad];
 
    // Initially make the captureSession object nil.
    _captureSession = nil;
     
    // Set the initial value of the flag to NO.
    _isReading = NO;
     
    [self startStopReading:nil];
}
 
- (IBAction)startStopReading:(id)sender {
    if (!_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        // The bar button item's title should change again.
    }
     
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}
 
#pragma mark - Private method implementation
 
- (BOOL)startReading{
    NSError *error;
     
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
     
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
     
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
     
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
     
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
     
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
 
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:scanView.layer.bounds];
    [scanView.layer addSublayer:_videoPreviewLayer];
     
    // Start video capture.
    [_captureSession startRunning];
     
    return YES;
}
 
-(void)stopReading :(AVMetadataMachineReadableCodeObject *)object{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
     
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];

    NSLog(@"object:%@",[object stringValue]);
    //此页面已经存在于self.navigationController.viewControllers中,并且是当前页面的前一页面
   NSLog(@"self.navigationController.viewControllers:%@",self.navigationController.viewControllers);
    oneViewController *preVC = [self.navigationController.viewControllers objectAtIndex:2];
    //初始化其属性
    preVC.scanString = @"" ;
    //传递参数过去
    preVC.scanString = [object stringValue];
    //使用popToViewController返回并传值到上一页面
    [self.navigationController popToViewController:preVC animated:YES];

}
- (IBAction)exitBtnAct:(UIButton *)sender {
    
    oneViewController *preVC = [self.navigationController.viewControllers objectAtIndex:2];
    [self.navigationController popToViewController:preVC animated:YES];
}
#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation
 
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
     
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil & [metadataObjects count] != 0) {
         
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeCode93Code]
            || [[metadataObj type] isEqualToString:AVMetadataObjectTypeCode128Code]
            || [[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            // 掃瞄完畢停止重複掃描
            dispatch_async(dispatch_get_main_queue(), ^{
                [self stopReading:[metadataObjects lastObject]];
            });
            _isReading = NO;
            
        }
    }
}
 -(void)alertControl :(NSString *)title :(NSString *)message {
     
     UIAlertController *alertController =
     [UIAlertController alertControllerWithTitle:title
                                         message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
     // Close
     UIAlertAction *alertAction =
     [UIAlertAction actionWithTitle:@"確定"
                              style:UIAlertActionStyleCancel
                            handler:nil];
     [alertController addAction:alertAction];
     [self presentViewController:alertController animated:YES completion:nil];
     
 }
@end
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
