//
//  ViewController.m
//  WebView封装
//
//  Created by Hanwen on 2017/12/28.
//  Copyright © 2017年 SK丿希望. All rights reserved.
//

#import "ViewController.h"
#import "twoViewController.h"
@interface ViewController ()

- (IBAction)login:(UIButton *)sender;
@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.navigationController pushViewController:[[twoViewController alloc] init] animated:YES];
//}

- (IBAction)login:(UIButton *)sender {
    
    UIStoryboard *storyboard ;
    
    if (storyboard == nil) {
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        twoViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
        [self.navigationController pushViewController:mainViewController animated:YES];
    }
}
@end
