//
//  mainViewController.m
//  WebView封装
//
//  Created by HowardHung on 2019/9/20.
//  Copyright © 2019 SK丿希望. All rights reserved.
//

#import "mainViewController.h"
#import "twoViewController.h"
#import "oneViewController.h"
#import "NetService.h"
#import "AFNetworking.h"
@interface mainViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button01;
@property (weak, nonatomic) IBOutlet UIButton *button02;
@property (weak, nonatomic) IBOutlet UIButton *button03;


- (IBAction)buttion01_act:(UIButton *)sender;
- (IBAction)buttom02_act:(UIButton *)sender;
- (IBAction)button03_act:(UIButton *)sender;

@end

@implementation mainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.toolbarHidden = YES ;
    
//    self.button01.layer.borderWidth = 1.0f;
//    self.button01.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.button02.layer.borderWidth = 1.0f;
//    self.button02.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.button03.layer.borderWidth = 1.0f;
//    self.button03.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.button01.clipsToBounds=YES;
    self.button01.layer.cornerRadius=5;
    self.button02.clipsToBounds=YES;
    self.button02.layer.cornerRadius=5;
    self.button03.clipsToBounds=YES;
    self.button03.layer.cornerRadius=5;
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO ;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttion01_act:(UIButton *)sender {
    
    NSString *urlString = @"https://myapp-dev.wistron.com/portalapi/api/Portal/GetToken";
    
    NSString *deviceId = @"3f502cb93f43dc152eede89957ecb108fd694aa1" ;
    NSString *userId = @"10710022" ;
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                deviceId,@"deviceId",
                                userId,@"userId", nil];

    NSString *json = [NetService dictionaryToJson:parameters];
    
NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
//create the Method "GET" or "POST"
[urlRequest setHTTPMethod:@"POST"];
//Convert the String to Data
NSData *data1 = [json dataUsingEncoding:NSUTF8StringEncoding];
[urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//Apply the data to the body
[urlRequest setHTTPBody:data1];

NSURLSession *session = [NSURLSession sharedSession];
NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if(httpResponse.statusCode == 200)
    {
        NSError *parseError = nil;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSLog(@"The response is - %@",responseDictionary);
        NSInteger rtncode = [[responseDictionary objectForKey:@"rtncode"] integerValue];
        
        if(rtncode == 1)
        {
                UIStoryboard *storyboard ;
            
                if (storyboard == nil) {
            
                    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                }
            oneViewController *wkwebview = [storyboard instantiateViewControllerWithIdentifier:@"wkwebview1"];

            NSLog(@"wkwebview:%@",wkwebview);
            NSString *token = [NSString stringWithFormat:@"%@",[responseDictionary valueForKey:@"data"]];
            NSLog(@"data:%@",token);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                wkwebview.token = [NSString stringWithFormat:@"%@",[responseDictionary valueForKey:@"data"]];
                [self.navigationController pushViewController:wkwebview animated:YES];

            });
            NSLog(@"Login SUCCESS");
        }
        else
        {
            NSLog(@"Login FAILURE");
        }
    }
    else
    {
        NSLog(@"Error");
    }
}];
[dataTask resume];
    
}
- (IBAction)buttom02_act:(UIButton *)sender {
    
    UIStoryboard *storyboard ;
    
    if (storyboard == nil) {
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        twoViewController *wkwebview = [storyboard instantiateViewControllerWithIdentifier:@"wkwebview2"];
        [self.navigationController pushViewController:wkwebview animated:YES];
    }
    
}
- (IBAction)button03_act:(UIButton *)sender {
    
}
@end
