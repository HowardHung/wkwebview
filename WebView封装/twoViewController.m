//
//  twoViewController.m
//  WebView封装
//
//  Created by Hanwen on 2017/12/28.
//  Copyright © 2017年 SK丿希望. All rights reserved.
//

#import "twoViewController.h"

@interface twoViewController ()

@property (strong, nonatomic) NSString  *someString;

- (IBAction)backBTN:(UIBarButtonItem *)sender;
- (IBAction)nextBTN:(UIBarButtonItem *)sender;

- (IBAction)reload:(UIBarButtonItem *)sender;
- (IBAction)js_function:(UIBarButtonItem *)sender;
- (IBAction)getelemtbyID:(UIBarButtonItem *)sender;
- (IBAction)changePic:(UIBarButtonItem *)sender;
- (IBAction)changeText:(UIBarButtonItem *)sender;
- (IBAction)insertJS_function:(UIBarButtonItem *)sender;
- (IBAction)settoken:(UIBarButtonItem *)sender;


@end

@implementation twoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO ;
    self.navigationController.toolbarHidden = NO ;
    self.someString = @"老舊的 UIWebView 存在的載入速度慢以及內存占用的問題，會限制住網頁方的自由度，若 App 支援的版本在 iOS 8 以上，建議使用 WKWebView，若在 iOS 8 以下，則使用 UIWebView，而這兩個元件的使用方式皆大同小異。";
    
    http://pmcsapd01:7880/
    self.url = [NSURL URLWithString:@"https://myapp-dev.wistron.com/WebDemo"];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]init];
    
    [self settoken:item];
}
// 随机生成字符串(由大小写字母组成)
- (IBAction)backBTN:(UIBarButtonItem *)sender {
    
    if ([self.webView canGoBack]) {
        
        NSLog(@"canGoBack");
        [self.webView goBack];
    }
}
- (IBAction)nextBTN:(UIBarButtonItem *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"登出" message:@"是否登出返回主頁" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)reload:(UIBarButtonItem *)sender {
    
    [self.webView reload];
}
- (IBAction)js_function:(UIBarButtonItem *)sender {
    
    [self.webView evaluateJavaScript:@"showAlert('hahahha')"  completionHandler:^(id item, NSError * _Nullable error) {
        NSLog(@"item:%@",item);
    }];
}

- (IBAction)getelemtbyID:(UIBarButtonItem *)sender {
    
    //替换第id为idtest的DIV元素内容
    NSString *tempString2 = [NSString stringWithFormat:@"document.getElementById('idTest').innerHTML ='%@';",self.someString];
    [self.webView evaluateJavaScript:tempString2  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];
}

- (IBAction)changePic:(UIBarButtonItem *)sender {
    
    NSString *tempString2 = [NSString stringWithFormat:@"document.getElementsByTagName('img')[0].src ='%@';",@"light_advice.png"];
    [self.webView evaluateJavaScript:tempString2  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];
}
- (IBAction)changeText:(UIBarButtonItem *)sender {
    
    NSString *tempString2 = [NSString stringWithFormat:@"document.getElementsByTagName('p')[0].style.fontSize='%@';",@"19px"];
    [self.webView evaluateJavaScript:tempString2  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];
}

- (IBAction)insertJS_function:(UIBarButtonItem *)sender {
    
    NSString *insertString = [NSString stringWithFormat:
                              @"var script = document.createElement('script');"
                              "script.type = 'text/javascript';"
                              "script.text = \"function jsFunc() { "
                              "var a=document.getElementsByTagName('body')[0];"
                              "alert('%@');"
                              "}\";"
                              "document.getElementsByTagName('head')[0].appendChild(script);", self.someString];
    
    NSLog(@"insert string %@",insertString);
    [self.webView evaluateJavaScript:insertString  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];
    
    [self.webView evaluateJavaScript:@"jsFunc();"  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];}

- (IBAction)settoken:(UIBarButtonItem *)sender {
    
    char data[ 32 ];
    for ( int x= 0 ;x< 32 ;data[x++] = ( char )( 'A' + ( arc4random_uniform ( 26 ))));
    NSString *string = [[ NSString alloc ] initWithBytes :data length : 32 encoding : NSUTF8StringEncoding ];
    
    NSData *plainData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
    NSLog(@"base64String:%@", base64String); // SGFwcHlNYW4=
    
    NSString *callJSstring = [NSString stringWithFormat:@"callJSFromApp('%@')",base64String];
    
    [self.webView evaluateJavaScript:callJSstring  completionHandler:^(id item, NSError * _Nullable error) {
        
    }];
}
// Webview銷毀的时候调用
- (void)dealloc {
    // Webview銷毀的时候调用
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    NSLog(@"webView離開");
}


//- (IBAction)getelemtbyTag:(UIButton *)sender {
//
//    //插入整个页面内容
//    // document.getElementsByTagName('body')[0];"
//    //替换第一个P元素内容
//    NSString *tempString = [NSString stringWithFormat:@"document.getElementsByTagName('p')[0].innerHTML ='%@';",self.someString];
//    [self.webView evaluateJavaScript:tempString  completionHandler:^(id item, NSError * _Nullable error) {
//
//    }];
//}
@end
